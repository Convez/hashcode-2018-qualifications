# HashCode 2018 - Qualification Round Solution

More info: https://hashcode.withgoogle.com

## About

This was the solution we developed during the qualification round, which unfortunately missed most of our ideas, resulting in a medriocre position in the final scoreboard. We later refined it during the extended round for a much better result: https://bitbucket.org/teamrucola/hashcode-2018-extended-round/

## Partecipants

* [Alessio Giuseppe Calì](https://www.linkedin.com/in/alessio-giuseppe-cali/)
* [Enrico Panetta](https://www.linkedin.com/in/enrico-panetta/)
* [Jeanpierre Francois](https://www.linkedin.com/in/jeanpierre-francois-7b2613129/)
* [Simone Leonardi](https://www.linkedin.com/in/simone-leonardi-42a6536b/)